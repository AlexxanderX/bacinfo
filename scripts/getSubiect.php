<?php

ob_start();

include("DatabaseConnectionFactory.php");
$connection = DatabaseConnectionFactory::getFactory()->getConnection();

$returnJSON = array();
$returnJSON["success"] = false;

$varianta = $_POST["varianta"];
$subiect = $_POST["subiect"];

if ($result = $connection->query("SELECT `Text` FROM `informatica` WHERE `Varianta`=$varianta AND `Subiect`=$subiect")) {
    if ($result->num_rows > 0) {
        $row = $result->fetch_row();
        $returnJSON["text"] = $row[0];

        $returnJSON["success"] = true;
    }
    else $returnJSON["error"] = $connection->error;
} else $returnJSON["error"] = $connection->error;


echo json_encode($returnJSON);
ob_end_flush();

?>
