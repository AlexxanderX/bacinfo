<?php

ob_start();

include("DatabaseConnectionFactory.php");
$connection = DatabaseConnectionFactory::getFactory()->getConnection();

$returnJSON = array();
$returnJSON["success"] = false;

$colorID = 0;
if (empty($_POST["hash"])) {
    $returnJSON["error"] = "";
    echo json_encode($returnJSON);
    ob_end_flush();
    die();
} else {
    $hashLine = strtok($_POST['text'], "\r\n");
    if ($hashLine !== "!#".$_POST["hash"]) {
        $returnJSON["error"] = "";
        echo json_encode($returnJSON);
        ob_end_flush();
        die();
    }
    else {
        $_POST["text"] = substr($_POST["text"], strlen($hashLine)+2);

        $colorLine = strtok("\r\n");
        if (substr($colorLine, 0, 6) === "@color") {
            $colorIDtmp = substr($colorLine, 9, 1);
            if (intval($colorIDtmp) < 4) {
                $colorID = intval($colorIDtmp);
                $returnJSON["color"] = $colorID;
            } else {
                $returnJSON["error"] = "color error " . $colorLine . ' ' . $colorIDtmp. ' ' . intval(is_int($colorIDtmp));
                echo json_encode($returnJSON);
                ob_end_flush();
                die();
            }
        }

        $_POST["text"] = substr($_POST["text"], strlen($colorLine)+2);
    }
}

if ($stmt = $connection->prepare("UPDATE `informatica` SET `Text`=?, `Culoare`=? WHERE `Varianta`=? AND `Subiect`=?"))
{
    $stmt->bind_param("siii", $_POST['text'], $colorID, $_POST['varianta'], $_POST['subiect']);
    $stmt->execute();

    if ($stmt->affected_rows != -1) $returnJSON["success"] = true;
    else $returnJSON["error"] = $connection->error;

    $stmt->close();
} else $returnJSON["error"] = $connection->error;

echo json_encode($returnJSON);

ob_end_flush();

?>
