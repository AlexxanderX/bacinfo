<?php

class DatabaseConnectionFactory {
    private static $factory;
    private $connection;

    private $servername = "";
    private $username = "";
    private $password = "";
    private $database = "";

    private function __connection() {}

    public static function getFactory() {
        if (!self::$factory) {
            self::$factory = new DatabaseConnectionFactory();
        }

        return self::$factory;
    }

    public function getConnection() {
        if (!$this->connection) {
            // Create connection
            $this->connection = new mysqli($this->servername, $this->username, $this->password, $this->database);

            // Check connection
            if ($this->connection->connect_error) {
                die("Connection failed: " . $this->connection->connect_error);
            }
        }

        return $this->connection;
    }
}

?>
