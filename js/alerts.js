function successAlert(message, time) {
    $("body").prepend("<div class=\"alert-dialog alert-success\">" + message + "</div>");
    window.setTimeout(function() {

    $(".alert-success").fadeTo(time/2, 0).slideUp(500, function(){
          $(this).remove();
      });
  }, time);
}

function errorAlert(message, time) {
    $("body").prepend("<div class=\"alert-dialog alert-error\">" + message + "</div>");
    window.setTimeout(function() {

    $(".alert-error").fadeTo(1000, 1).slideUp(300, function(){
          $(this).remove();
      });
  }, time);
}

function alert(type, message) {
    var classType = "alert-" + type;
    $("body").prepend("<div class=\"alert-dialog " + classType + "\">" + message + "</div>");
    window.setTimeout(function() {

    $("." + classType).fadeTo(1000, 1).slideUp(300, function(){
          $(this).remove();
      });
  }, 2500);
}
