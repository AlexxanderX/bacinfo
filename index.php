<?php
session_start();
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
    // last request was more than 30 minutes ago
    session_unset();     // unset $_SESSION variable for the run-time
    session_destroy();   // destroy session data in storage
    header("Location: index.php");
}
$_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
?>

<!DOCTYPE>
<html lang='ro'>
<head>
    <meta charset='UTF-8'>

    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet'  type='text/css'>
    <link rel='stylesheet' type='text/css' href='css/bootstrap.min.css'>
    <link rel='stylesheet' type='text/css' href='css/bootstrap-theme.min.css'>
    <link rel='stylesheet' type='text/css' href='css/general.css'>
    <link rel='stylesheet' type='text/css' href='css/tabel.css'>

    <script src='js/jquery-2.1.4.min.js'></script>
    <script src='js/bootstrap.min.js'></script>
    <script src='js/bPopup.min.js'></script>
    <script src='js/alerts.js'></script>
    <script src='js/jquery.hotkeys.js'></script>
    <script src='js/sha256.js'></script>

    <script>
        $(document).ready(function(){
            var popup;
            var col;
            var row;
            var hash = "";

            $(".subiectButton").click(function(event){
                event.preventDefault();

                col = $(this).parent().parent().children().index($(this).parent());
                row = $(this).parent().parent().parent().children().index($(this).parent().parent());

                $("#see").val("");

                var formData = new FormData();
                formData.append("varianta", row);
                formData.append("subiect", col);

                $.ajax({
                    type: "POST",
                    url: "scripts/getSubiect.php",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: "json"
                }).done(function(data) {
                    if (data["success"] === true) {
                        $("#see").val(data["text"]);
                    }
                    $("#subiectContainer").bPopup();
                });
            });

            $(document).delegate('#see', 'keydown', function(e) {
              var keyCode = e.keyCode || e.which;

              if (keyCode == 9) {
                e.preventDefault();
                var start = $(this).get(0).selectionStart;
                var end = $(this).get(0).selectionEnd;

                // set textarea value to: text before caret + tab + text after caret
                $(this).val($(this).val().substring(0, start)
                            + "\t"
                            + $(this).val().substring(end));

                // put caret at right position again
                $(this).get(0).selectionStart =
                $(this).get(0).selectionEnd = start + 1;
              }
            });

            $(document).bind("keydown", "ctrl+z", function(){
                hash = CryptoJS.SHA256(new Date().getTime().toString());
                successAlert(hash, 2500);
            });

            $("#see").bind("keydown", "ctrl+x", function(){
                var formData = new FormData();
                formData.append("varianta", row);
                formData.append("subiect", col);
                formData.append("text", $("#see").val());
                formData.append("hash", hash);

                $.ajax({
                    type: "POST",
                    url: "scripts/updateSubiect.php",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: "json"
                }).done(function(data) {
                    if (data["success"] === true) {
                        successAlert("<strong>Success!</strong> Textul a fost salvat.", 2500);
                        $("#see").val($('#see').val().replace(/^.*\n/g,""));
                        $("#see").val($('#see').val().replace(/^.*\n/g,""));

                        if (data["color"] !== undefined) {
                            $('table tr:eq(' + row + ') td:eq(' + col + ') .subiectButton').removeClass(function(index, className) { return className.replace(/(^|\s)+subiectButton\s+/, ''); });
                            var colors = ['btn-info', 'btn-warning', 'btn-success', 'btn-danger'];
                            console.log(colors[data["color"]]);
                            $('table tr:eq(' + row + ') td:eq(' + col + ') .subiectButton').addClass("subiectButton btn " + colors[data["color"]]);
                        }
                    } else {
                        errorAlert("<strong>Eroare!</strong> " + data["error"], 2500);
                    }
                });
            });
        });
    </script>
</head>
<body>
    <div class="tabel">
        <table>
        <tr class="header">
            <th>Varianta</th>
            <th>Subiectul 1</th>
            <th>Subiectul 2</th>
            <th>Subiectul 3</th>
        </tr>
        <?php
            include ("scripts/DatabaseConnectionFactory.php");
            $connection = DatabaseConnectionFactory::getFactory()->getConnection();

            if ($result = $connection->query("SELECT `Culoare`, `Varianta`, `Subiect` FROM `informatica` ORDER BY Varianta, Subiect")) {
                if ($result->num_rows > 0) {
                    $i = 0;
                    $colors = array('btn-info', 'btn-warning', 'btn-success', 'btn-danger');
                    while ($row = $result->fetch_row()) {
                        if ($row[1] != $i) {
                            $i = $row[1];
                            echo "</tr>\n<tr><td>Varianta $i</td>";
                        }
                        echo "<td><a href='#' class='subiectButton btn " . $colors[$row[0]] . "' role='button'>Subiectul " . $row[2] . "</a></td>";
                    }
                } else echo $connection->error;
            } else echo $connection->error;
        ?>
        </table>
    </div>

    <div id="subiectContainer" class="bPopupDialog" style="padding: 5px 5px;">
        <textarea id="see"></textarea>
        <?php if (isset($_SESSION["username"]) && $_SESSION["username"] == "AlexxanderX") {
            echo "<a href='#' class='editareButton btn btn-warning' role='button'>Actualizare</a>";
        }
        ?>
    </div>
</body>
</html>
